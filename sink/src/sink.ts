import { Client, Msg } from 'ts-nats';

import natsConnection from 'src/nats';
import redis from 'src/redis';

async function updateField(id: string, field: string, payload: string): Promise<void> {
  const userString: string = await redis.get(id);
  const updatedUser: any = JSON.parse(userString);
  updatedUser[field] = payload;
  await redis.set(id, JSON.stringify(updatedUser));
}
async function setupSink(): Promise<any> {
  console.log('🚀 Sink ready');
  const nats: Client = await natsConnection;

  /// hackathon
  // new.hackathon => id
  // hackathon.<id> => inclui o name
  // delete.hackathon => id
  nats.subscribe('new.hackathon', async (_: any, msg: Msg) => {
    const hackathonCreated: any = JSON.parse(msg.data);
    const id: string = hackathonCreated.payload;
    console.log(id);
    await redis.set(id, JSON.stringify({ id }));
  nats.subscribe(`hackathon.${id}`, async (__: any, userMsg: Msg) => {
      const { type, payload }: any = JSON.parse(userMsg.data);
      console.log(`Type:${type} payload: ${payload}`);
      // preciso achar qual updateHackathon foi encontrado
      // updateHackathonName => atualiza o nome
      // updateHackathonUser => atualiza o user
      const field: string = type
        .replace('updatedHackathon', '')
        .toLowerCase();
      console.log(`type: ${type}`);
      await updateField(id, field, payload);
    });
  });
  nats.subscribe('delete.hackathon', async(_:any, msg: Msg) => {
    const hackathonCreated: any = JSON.parse(msg.data);
    console.log(hackathonCreated);
    const id: string = hackathonCreated.payload;
    console.log(`id: ${id} payload: ${hackathonCreated.payload}`);    
    // remove no redis
  });
  
  nats.subscribe('new.user', async (_: any, msg: Msg) => {
    const userCreated: any = JSON.parse(msg.data);
    const id: string = userCreated.payload;

    await redis.set(id, JSON.stringify({ id }));
  nats.subscribe(`user.${id}`, async (__: any, userMsg: Msg) => {
      const { type, payload }: any = JSON.parse(userMsg.data);
      const field: string = type
        .replace('updatedUser', '')
        .toLowerCase();

      await updateField(id, field, payload);
    });
  });
}






setupSink();
