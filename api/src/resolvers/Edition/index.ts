
import { Context } from 'koa';

import { Edition } from 'src/resolvers/Edition/types';

// Queries
import getEdition from 'src/resolvers/Edition/getEdition';

// Mutations
import createEdition from 'src/resolvers/Edition/createEdition';
import deleteEdition from 'src/resolvers/Edition/deleteEdition';
import updateEdition from 'src/resolvers/Edition/updateEdition';

export const EditionQuery: any = {
  getEdition(_: any, args: any, context: Context): Promise<Edition> {
    return getEdition(args, context);
  },
};

export const EditionMutation: any = {
  createEdition(_: any, args: any, context: Context): Promise<Edition> {
    return createEdition(args, context);
  },
  updateEdition(_: any, args: any, context: Context): Promise<Edition> {
    return updateEdition(args, context);
  },
  deleteEdition(_: any, args: any, context: Context): Promise<Edition> {
    return deleteEdition(args, context);
  },

};
