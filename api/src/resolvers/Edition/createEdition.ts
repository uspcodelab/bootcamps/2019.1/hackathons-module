import { Context } from 'koa';

import { Edition, EditionInput, EditionInputData } from 'src/resolvers/Edition/types';

import Event from 'src/resolvers/event';

async function createEdition(args: EditionInput, context: Context): Promise<Edition> {
  const {
    versionName,
    date,
    theme,
    description,
    rules,
    preRequirement,
    subscriptions,
    location,
    judgingCriteria,
  }: EditionInputData = args.edition;

  const id: string = '';
  const idHackathon: string = '';

  const edition: Edition = {
    id,
    idHackathon,
    versionName,
    date,
    theme,
    description,
    rules,
    preRequirement,
    subscriptions,
    location,
    judgingCriteria,
  };

  const createdEdition: Event =
    new Event ('createdEdition', edition.id);

  const updatedEditionHackathon: Event =
    new Event ('updatedEditionHackathon', edition.idHackathon);

  const updatedEditionName: Event =
    new Event ('updatedEditionName', edition.versionName);

  const updatedEditionDate: Event =
    new Event ('updatedEditionDate', edition.date);

  const updatedEditionTheme: Event =
    new Event ('updatedEditionTheme', edition.theme);

  const updatedEditionDescription: Event =
    new Event ('updatedEditionDescription', edition.description);

  const updatedEditionRules: Event =
    new Event ('updatedEditionRules', edition.rules);

  const updatedEditionPreRequirement: Event =
    new Event ('updatedEditionPreRequirement', edition.preRequirement);

  const updatedEditionSubscriptions: Event =
    new Event ('updatedEditionSubscriptions', edition.subscriptions);

  const updatedEditionLocation: Event =
    new Event ('updatedEditionLocation', edition.location);

  const updatedEditionJudgingCriteria: Event =
    new Event ('updatedEditionJudgingCriteria', edition.judgingCriteria);

  context.nats.publish(`new.edition`, JSON.stringify(createdEdition));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionHackathon));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionName));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionDate));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionTheme));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionDescription));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionRules));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionPreRequirement));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionSubscriptions));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionLocation));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionJudgingCriteria));

  return edition;
}

export default createEdition;
