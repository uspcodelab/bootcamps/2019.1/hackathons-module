import { Context } from 'koa';

import { Edition } from 'src/resolvers/Edition/types';
import Event from 'src/resolvers/event';

async function deleteEdition({ id }: Edition, context: Context): Promise<Edition> {

  const editionRedis: string = await context.redis.getAsync(id);
  if (editionRedis === null) {

    return {} as Edition;
  }

  const edition: Edition = JSON.parse(editionRedis);
  const deleteEditionEvent: Event = new Event('deleteEdition', id);
  context.nats.publish('delete.edition', JSON.stringify(deleteEditionEvent));

  return edition;

}

export default deleteEdition;
