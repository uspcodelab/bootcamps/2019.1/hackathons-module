import {Context} from 'koa';

import { Edition, EditionQuery } from 'src/resolvers/Edition/types';

async function getEdition({ id }: EditionQuery, context: Context): Promise<Edition> {
  const editionString: string = await context.redis.getAsync(id);
  const edition: Edition = JSON.parse(editionString);
  console.log(editionString);

  return edition;
}
export default getEdition;
