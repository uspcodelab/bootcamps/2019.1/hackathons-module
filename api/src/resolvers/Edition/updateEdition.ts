import { Context } from 'koa';

import { Edition } from 'src/resolvers/Edition/types';

import Event from 'src/resolvers/event';

async function updateEdition(args: Edition, context: Context): Promise<Edition> {
  const {
    id,
    idHackathon,
    versionName,
    date,
    theme,
    description,
    rules,
    preRequirement,
    subscriptions,
    location,
    judgingCriteria,
  }: Edition = args;

  const edition: Edition = {
    id,
    idHackathon,
    versionName,
    date,
    theme,
    description,
    rules,
    preRequirement,
    subscriptions,
    location,
    judgingCriteria,
  };

  const updatedEditionName: Event =
    new Event ('updatedEditionName', edition.versionName);

  const updatedEditionDate: Event =
    new Event ('updatedEditionDate', edition.date);

  const updatedEditionTheme: Event =
    new Event ('updatedEditionTheme', edition.theme);

  const updatedEditionDescription: Event =
    new Event ('updatedEditionDescription', edition.description);

  const updatedEditionRules: Event =
    new Event ('updatedEditionRules', edition.rules);

  const updatedEditionPreRequirement: Event =
    new Event ('updatedEditionPreRequirement', edition.preRequirement);

  const updatedEditionSubscriptions: Event =
    new Event ('updatedEditionSubscriptions', edition.subscriptions);

  const updatedEditionLocation: Event = new Event ('updatedEditionLocation', edition.location);

  const updatedEditionJudgingCriteria: Event = new Event ('updatedEditionJudgingCriteria', edition.judgingCriteria);
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionName));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionDate));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionTheme));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionDescription));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionRules));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionPreRequirement));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionSubscriptions));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionLocation));
  context.nats.publish(`edition.${edition.id}`, JSON.stringify(updatedEditionJudgingCriteria));

  return edition;
}

export default updateEdition;
