interface Date {
  ini: string;
  end: string;
}

interface Judging {
  name: string;
  description: string;
}

interface Prize {
  id: string;
  prize: string[];
}


export interface Edition {
  id: string;
  idHackathon: string;
  versionName: string;
  date: Date;
  theme: string;
  description: string;
  rules: string[];
  preRequirement: string[];
  subscriptions: Date;
  location: string;
  judgingCriteria: Judging[];
}

interface EditionInputData {
  versionName: string;
  date: Date;
  theme: string;
  description: string;
  rules: string[];
  preRequirement: string[];
  subscriptions: Date;
  location: string;
  judgingCriteria: Judging[];
}

export interface EditionInput {
	edition: EditionInputData;
}

export interface EditionQuery {
  id: string;
}
