import { Context } from 'koa';
import Event from 'src/resolvers/event';
import { Prize } from 'src/resolvers/Prize/types';

async function deletePrize({ id }: Prize, context: Context): Promise<Prize> {

    const prizeString: string = await context.redis.getAsync(id);
    if (prizeString === null)  {
        return {} as Prize;
    }
    const prize: Prize = JSON.parse(prizeString);
    const deletedPrize: Event = new Event('deletedPrize', prize.id);
    context.nats.publis(`delete.prize`, JSON.stringify(deletedPrize));

    return prize;
}

export default deletePrize;
