export interface Prize {
    id: string;
    rank: number;
    description: string;
    idEdition: string;
}
interface PrizeInputData {
    id: string;
    rank: number;
    description: string;
    idEdition: string;
}
export interface PrizeInput {
    prize: PrizeInputData;
}

export interface PrizeQuery{
    edition: string;
}

