import { Context } from 'koa';

import { Prize } from 'src/resolvers/Prize/types';
import createPrize from './createPrize';
import deletePrize from './deletePrize';
import getPrize from './getPrize';

export const PrizeQuery: any = {
    getPrize(_: any, args: any, context: Context): Promise<Prize> {
        return getPrize(args, context);
    },
};
export const PrizeMutation: any = {
    createPrize(_: any, args: any, context: Context): Promise<Prize> {
        return createPrize(args, context);
    },
    deletePrize(_: any, args: any, context: Context): Promise<Prize> {
        return deletePrize(args, context);
    },
};
