import { Context } from 'koa';

import { Prize } from 'src/resolvers/Prize/types';

async function getPrize({ id }: Prize, context: Context): Promise<Prize> {
    const prizeString: string = await context.redis.getAsync(id);
    const prize: Prize = JSON.parse(prizeString);

    return prize;
}

export default getPrize;
