import {Context } from 'koa';

import { Prize, PrizeInput, PrizeInputData} from 'src/resolvers/Prize/types';

import Event from 'src/resolvers/event';

async function createPrize(args: PrizeInput, context: Context): Promise<Prize> {
    const { rank, description, idEdition }: PrizeInputData = args.prize;

    const uuidPrize: any = require('uudi/v1');

    const prize: Prize = {
        id: uuidPrize(),
        idEdition,
        rank,
        description,
    };

    const newPrize: Event = new Event('createPrize', prize.id);
    const updatePrizeEdition: Event = new Event('updatePrizeEdition', prize.idEdition);
    const updatePrizeRank: Event = new Event ('updatePrizeRank', prize.rank);
    const updatePrizeDesc: Event = new Event ('updatePrizeDescription', prize.description);
    context.nats.publish(`new.prize`, JSON.stringify(newPrize));
    context.nats.publish(`prize.${prize.id}`, JSON.stringify(updatePrizeEdition));
    context.nats.publish(`prize.${prize.id}`, JSON.stringify(updatePrizeRank));
    context.nats.publish(`prize.${prize.id}`, JSON.stringify(updatePrizeDesc));

    return prize;
}

export default createPrize;
