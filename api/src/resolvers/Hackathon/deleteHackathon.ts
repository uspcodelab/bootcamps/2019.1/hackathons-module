/*
    Remover uma hackathon pelo <nome>
    - validacao:
       - ver se esse hackathon existe?
        Sim => remove (soft delete, remove a referencia dele)
       - (perguntar: a gente precisa checar as permissoes do usuario para fazer essa operacao???
    - Pegar a hackathon pelo nome
    - publicar a remocao dessa hackathon no nats
*/
import { Context } from 'koa';
import Event from 'src/resolvers/event';
import { Hackathon } from 'src/resolvers/Hackathon/types';

async function deleteHackathon({ id }: Hackathon  , context: Context): Promise<Hackathon> {

    // busco id do hackathon no redis
    const hackathonString: string  = await context.redis.getAsync(id);
    if (hackathonString === null) { // nao existe esse id no redis
        return {} as Hackathon;
    }
    const hackathon: Hackathon = JSON.parse(hackathonString);
    const deletedHackathonEvent: Event = new Event('deletedHackathon', hackathon.id);
    context.nats.publish(`delete.hackathon`, JSON.stringify(deletedHackathonEvent));

    // pode voltar null, caso nao exista
    return hackathon;
}

export default deleteHackathon;
