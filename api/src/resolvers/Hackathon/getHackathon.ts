/*
    pega o <nome> da hackathon e retorna um
    validacoes:
        - verificar se hackathon existe?
            Sim => retorna
            nao => erro ou retorna vazia (provavelmente o ultimo)

    - leitura direto do redis (pega pelo nome / id?)
*/

import { Context } from 'koa';

import { Hackathon, HackathonQueryByName } from 'src/resolvers/Hackathon/types';

async function getHackathon({ name }: HackathonQueryByName, context: Context): Promise<Hackathon> {

    // o get retorna nil se não existir, ai a string fica nula assim como o hackathon
    const hackathonString: string = await context.redis.getAsync(name);
    const hackathon: Hackathon = JSON.parse(hackathonString);

    // pode voltar null, caso nao exista
    return hackathon;
}

export default getHackathon;
