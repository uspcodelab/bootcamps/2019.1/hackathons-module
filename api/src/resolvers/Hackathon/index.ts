// arquivo com declaracoes externas das queries/mutacoes
// (./User/index.ts)
import { Context } from 'koa';

import { Hackathon } from 'src/resolvers/Hackathon/types';

import createHackathon from './createHackathon';
import deleteHackathon from './deleteHackathon';
import getHackathon from './getHackathon';
import updateHackathon from './updateHackathon';

export const HackathonQuery: any = {
    getHackathon(_: any, args: any, context: Context): Promise<Hackathon> {
        return getHackathon(args, context);
    },
};

export const HackathonMutation: any = {
    deleteHackathon(_: any, args: any, context: Context): Promise<Hackathon> {
        return deleteHackathon(args, context);
    },
    createHackathon(_: any, args: any, context: Context): Promise<Hackathon> {
        return createHackathon(args, context);
    },
    updateHackathon(_: any, args: any, context: Context): Promise<Hackathon> {
        return updateHackathon(args, context);
    },
};
