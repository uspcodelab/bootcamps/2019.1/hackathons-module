// resolver de criar uma hackathon
/* Lembrando:
  - Validacoes:
    - verificar se ja existe uma hackathon com o mesmo nome?
        Sim => erro
        Nao => ok, publica no NATS
   - (nao sabemos: precisa saber se ele eh um organizador ou nao ???)
  - Antes de publicar:
   - (por enquanto nao fazer) pegar o usuario atual (ID)
   - (por enquanto nao fazer) pegar o PWA atual  (ID)
   - criar uuid pro hackathon nova
   - adicionar essas infos no tipo OBjeto/Tipo Hackathon
   - nesse momento, edicoes eh um vetor vazio (nao foi criado mas eh um vetor)
   - Publicar:
    - criado o objeto, publicar no nats
    Esse evento publica no NATS, entao escrever:
        - publicar a escrita disso no nats
        - publicar a leitura do nats pra gravar no redis
*/
import { Context } from 'koa';

import { Hackathon, HackathonInput, HackathonInputData } from 'src/resolvers/Hackathon/types';

import Event from 'src/resolvers/event';

async function createHackathon(args: HackathonInput, context: Context): Promise<Hackathon> {
  // dados do hackathon (id usuario, name)
  const { idUser, name }: HackathonInputData = args.hackathon;

  // ver se o nome nao existe no redis
  // "hackathon" eh uma string fixa
  // TODO: lembrar na hora que fizer o sink de utilizar essa mesma string na hora de gravar (hackathon.name)
  const hackathonName: number = await context.redis.sismember('hackathon.name',  name);
  if (hackathonName === 1) {  // existe no set
    return {id: '', name: '', idUser: ''};
  } // hackathonName === 0 (nao existe no set)
  // TODO: remover esse require daqui pq ta feio
  const uuidHackathon: any = require('uuid/v1');
  // criando objeto hackathon
  const hackathon: Hackathon = {
    id: uuidHackathon(),
    name,
    idUser,
};
  // eventos relacionados ao hackathon
  const createdHackathon: Event =  new Event('createdHackathon', hackathon.id);
  const updatedHackathonName: Event = new Event('updatedHackathonName', hackathon.name);
  const updatedHackathonUser: Event = new Event('updatedHackathonUser', hackathon.idUser);

  // publicando esses eventos no nats
  context.nats.publish(`new.hackathon`, JSON.stringify(createdHackathon));
  // esses eventos ainda nao tenho certeza pra que serve
  context.nats.publish(`hackathon.${hackathon.id}`, JSON.stringify(updatedHackathonName));
  context.nats.publish(`hackathon.${hackathon.id}`, JSON.stringify(updatedHackathonUser));
  return hackathon;
}

export default createHackathon;
