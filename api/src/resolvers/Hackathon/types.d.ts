// tipos que vamos utilizar dentro dos resolvers 
// (tipos em typescript)
export interface Hackathon {
    id: string;
    name: string;
    idUser: string; 
//     pwa: string; // comentado porque nao sabemos a interface
}
interface HackathonInputData {
    idUser: string;
    name: string;
}
export interface HackathonInput {
    hackathon: HackathonInputData;
}

export interface HackathonQueryByName {
    name: string;
}