// resolver de criar uma hackathon
/* Lembrando:
  - Validacoes:
    - verificar se ja existe uma hackathon com o mesmo nome?
        Sim => erro
        Nao => ok, publica no NATS
   - (nao sabemos: precisa saber se ele eh um organizador ou nao ???)
  - Antes de publicar:
   - (por enquanto nao fazer) pegar o usuario atual (ID)
   - (por enquanto nao fazer) pegar o PWA atual  (ID)
   - criar uuid pro hackathon nova
   - adicionar essas infos no tipo OBjeto/Tipo Hackathon
   - nesse momento, edicoes eh um vetor vazio (nao foi criado mas eh um vetor)
   - Publicar:
    - criado o objeto, publicar no nats
    Esse evento publica no NATS, entao escrever:
        - publicar a escrita disso no nats
        - publicar a leitura do nats pra gravar no redis
*/
import { Context } from 'koa';

import { Hackathon } from 'src/resolvers/Hackathon/types';

import Event from 'src/resolvers/event';

async function updateHackathon(hackathon: Hackathon, context: Context): Promise<Hackathon> {

    // verifico se existe esse hackathon antes de remover
    const hackathonRedis: string = await context.redis.getAsync(hackathon.id);
    if (hackathonRedis === null) {
        return {id: '', name: '', idUser: ''};
    }
    // eventos relacionados ao hackathon
    const updateHackathonName: Event = new Event('updateHackathonName', hackathon.name);
    context.nats.publish(`update.hackathon`, JSON.stringify(hackathon.id));
    context.nats.publish(`hackathon.${hackathon.id}`, JSON.stringify(updateHackathonName));
    return hackathon;
}

export default updateHackathon;
