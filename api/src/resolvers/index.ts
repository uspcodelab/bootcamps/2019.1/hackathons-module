import { EditionMutation, EditionQuery } from 'src/resolvers/Edition';
import { HackathonMutation, HackathonQuery  } from 'src/resolvers/Hackathon';
import { PrizeMutation, PrizeQuery } from 'src/resolvers/Prize';

export default {
  Query: {
    ...HackathonQuery,
    ...PrizeQuery,
    ...EditionQuery,
  },
  Mutation: {
    ...HackathonMutation,
    ...PrizeMutation,
    ...EditionMutation,
  },
};
